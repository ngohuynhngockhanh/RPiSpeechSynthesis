﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PubnubWindowsStore
{
    public class PubnubConfigData
    {
        public bool ssl;
        public bool resumeOnReconnect;
        public string publishKey = "pub-c-c99c22c9-0406-4fa3-80a8-3e31aa3ed058";
        public string subscribeKey = "sub-c-fccaae06-a402-11e5-b58d-0619f8945a4f";
        public string cipherKey = "";
        public string secretKey = "sec-c-MzViOTk1OTItNzljNC00YmQwLWFjNjctYjMxYzIxOTM2NWFl";
        public string sessionUUID = "";
        public string origin = "";
        public string authKey = "";
        public bool hideErrorCallbackMessages;

        public int subscribeTimeout;
        public int nonSubscribeTimeout;
        public int maxRetries;
        public int retryInterval;
        public int localClientHeartbeatInterval;
        public int presenceHeartbeat;
        public int presenceHeartbeatInterval;
    }
}
