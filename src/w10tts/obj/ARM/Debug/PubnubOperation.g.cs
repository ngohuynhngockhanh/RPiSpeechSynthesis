﻿#pragma checksum "C:\Users\KSP\workspace\RPiSpeechSynthesis\src\w10tts\PubnubOperation.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "FFD2ECA39CED967270778ECFD9F032D8"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PubnubWindowsStore
{
    partial class PubnubOperation : 
        global::Windows.UI.Xaml.Controls.Page, 
        global::Windows.UI.Xaml.Markup.IComponentConnector,
        global::Windows.UI.Xaml.Markup.IComponentConnector2
    {
        /// <summary>
        /// Connect()
        /// </summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 14.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void Connect(int connectionId, object target)
        {
            switch(connectionId)
            {
            case 1:
                {
                    this.pageRoot = (global::Windows.UI.Xaml.Controls.Page)(target);
                    #line 11 "..\..\..\PubnubOperation.xaml"
                    ((global::Windows.UI.Xaml.Controls.Page)this.pageRoot).Unloaded += this.Page_Unloaded;
                    #line default
                }
                break;
            case 2:
                {
                    this.txtChannel = (global::Windows.UI.Xaml.Controls.TextBox)(target);
                }
                break;
            case 3:
                {
                    this.txtChannelGroup = (global::Windows.UI.Xaml.Controls.TextBox)(target);
                }
                break;
            case 4:
                {
                    global::Windows.UI.Xaml.Controls.Button element4 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 58 "..\..\..\PubnubOperation.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element4).Click += this.btnSubscribe_Click;
                    #line default
                }
                break;
            case 5:
                {
                    global::Windows.UI.Xaml.Controls.Button element5 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 59 "..\..\..\PubnubOperation.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element5).Click += this.btnPresence_Click;
                    #line default
                }
                break;
            case 6:
                {
                    global::Windows.UI.Xaml.Controls.Button element6 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 60 "..\..\..\PubnubOperation.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element6).Click += this.btnPublish_Click;
                    #line default
                }
                break;
            case 7:
                {
                    global::Windows.UI.Xaml.Controls.Button element7 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 62 "..\..\..\PubnubOperation.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element7).Click += this.btnHistory_Click;
                    #line default
                }
                break;
            case 8:
                {
                    global::Windows.UI.Xaml.Controls.Button element8 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 63 "..\..\..\PubnubOperation.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element8).Click += this.btnGlobalHereNow_Click;
                    #line default
                }
                break;
            case 9:
                {
                    global::Windows.UI.Xaml.Controls.Button element9 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 64 "..\..\..\PubnubOperation.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element9).Click += this.btnHereNow_Click;
                    #line default
                }
                break;
            case 10:
                {
                    global::Windows.UI.Xaml.Controls.Button element10 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 66 "..\..\..\PubnubOperation.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element10).Click += this.btnUnsubscribe_Click;
                    #line default
                }
                break;
            case 11:
                {
                    global::Windows.UI.Xaml.Controls.Button element11 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 67 "..\..\..\PubnubOperation.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element11).Click += this.btnPresenceUnsub_Click;
                    #line default
                }
                break;
            case 12:
                {
                    global::Windows.UI.Xaml.Controls.Button element12 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 68 "..\..\..\PubnubOperation.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element12).Click += this.btnTime_Click;
                    #line default
                }
                break;
            case 13:
                {
                    global::Windows.UI.Xaml.Controls.Button element13 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 70 "..\..\..\PubnubOperation.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element13).Click += this.btnPAMChannel_Click;
                    #line default
                }
                break;
            case 14:
                {
                    global::Windows.UI.Xaml.Controls.Button element14 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 71 "..\..\..\PubnubOperation.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element14).Click += this.btnPAMChannelGroup_Click;
                    #line default
                }
                break;
            case 15:
                {
                    global::Windows.UI.Xaml.Controls.Button element15 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 78 "..\..\..\PubnubOperation.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element15).Click += this.btnUserState_Click;
                    #line default
                }
                break;
            case 16:
                {
                    global::Windows.UI.Xaml.Controls.Button element16 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 80 "..\..\..\PubnubOperation.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element16).Click += this.btnWhereNow_Click;
                    #line default
                }
                break;
            case 17:
                {
                    global::Windows.UI.Xaml.Controls.Button element17 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 81 "..\..\..\PubnubOperation.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element17).Click += this.btnChangeUUID_Click;
                    #line default
                }
                break;
            case 18:
                {
                    global::Windows.UI.Xaml.Controls.Button element18 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 82 "..\..\..\PubnubOperation.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element18).Click += this.btnChannelGroup_Click;
                    #line default
                }
                break;
            case 19:
                {
                    global::Windows.UI.Xaml.Controls.Button element19 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 84 "..\..\..\PubnubOperation.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element19).Click += this.btnDisconnectRetry_Click;
                    #line default
                }
                break;
            case 20:
                {
                    global::Windows.UI.Xaml.Controls.Button element20 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 85 "..\..\..\PubnubOperation.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element20).Click += this.btnDisableNetwork_Click;
                    #line default
                }
                break;
            case 21:
                {
                    global::Windows.UI.Xaml.Controls.Button element21 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 86 "..\..\..\PubnubOperation.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element21).Click += this.btnEnableNetwork_Click;
                    #line default
                }
                break;
            case 22:
                {
                    this.txtResult = (global::Windows.UI.Xaml.Controls.TextBox)(target);
                }
                break;
            case 23:
                {
                    global::Windows.UI.Xaml.Controls.Button element23 = (global::Windows.UI.Xaml.Controls.Button)(target);
                    #line 88 "..\..\..\PubnubOperation.xaml"
                    ((global::Windows.UI.Xaml.Controls.Button)element23).Click += this.btnHome_Click;
                    #line default
                }
                break;
            case 24:
                {
                    this.backButton = (global::Windows.UI.Xaml.Controls.Button)(target);
                }
                break;
            case 25:
                {
                    this.pageTitle = (global::Windows.UI.Xaml.Controls.TextBlock)(target);
                }
                break;
            default:
                break;
            }
            this._contentLoaded = true;
        }

        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.Windows.UI.Xaml.Build.Tasks"," 14.0.0.0")]
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public global::Windows.UI.Xaml.Markup.IComponentConnector GetBindingConnector(int connectionId, object target)
        {
            global::Windows.UI.Xaml.Markup.IComponentConnector returnValue = null;
            return returnValue;
        }
    }
}

